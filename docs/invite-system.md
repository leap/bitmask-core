# Invite system

## Purpose

In a censored environment, access to menshen (API) or VPN gateways can be blocked by a nation state actor (e.g. by blocking ip addresses). Invite codes can be used to circumvent censorship. An invite code is an url containing basically two things:

- a private introducer proxy to access menshen (network traffic to proxy can be obfuscated, e.g. by using obfs4)
- credentials for menshen to get private resources (like non-public VPN gateways)

The invite code also has a field `fqdn` which is used as menshen hostname. There are endpoints in menshen returning the whole provider configuration. So you can also add an invite token to use a custom provider.

## Wording

Invite code describes the whole url with all the information. Invite token is the credential part of the invite code. In the bitmask-core code the invite code is named "introducer url".


## User workflow

Technically, an invite code is an url. It needs to be shared to the users by the provider. This part is not handled by LEAP VPN. When starting the LEAP VPN client for the first time, the user can

- choose from a pre-configured list of providers
- add a custom provider (url)
- add an invite code by copy/pasting/entering text or scanning a qr code

LEAP VPN clients also registers special url handlers like `obfsvpnintro://`. When a user clicks on such a link the client opens and the invite code can be added. After adding an invite code, the user ends up in automatically using obfuscated communication.

The use of an invite token should bypass protocol and ip filtering. Therefore private resources need to be private.

## Provider workflow

To support invite codes, a provider needs to deploy some private resources. A resource can be non-public VPN gateways or bridges.

1) A provider needs to host a private introducer proxy to access menshen (https://0xacab.org/leap/obfsvpn/-/issues/66)
2) Resources can be grouped by a bucket (https://0xacab.org/leap/menshen/-/issues/61#note_1228104)
3) Access token can be generated to access one or more buckets (docs: https://0xacab.org/leap/menshen/#bucket-token-auth). With an access token, menshen returns private resources belonging to a bucket.
4) The provider needs to generate invite codes and distribute them to the users (https://0xacab.org/leap/menshen/-/issues/61#note_1228102)

## Fields of an introducer (obfsvpnintro)

Let's take this example:

> obfsvpnintro://private-menshen.solitech.net:4430/?cert=MpRqHUJtZuL1ZUB3agXPGJJ0aZTIc20N5haBkAllf2CaTnaWOAtJGfUCgv0XCOJzt1qpfQ&fqdn=public-menshen.solitech.net&kcp=0&auth=solitech_e9JckBd9QzYmSE4wo2jQBQ==

**Explanation:**

- `public-menshen.solitech.net` is the public menshen instance that easily can be blocked
- [bitmask-core](https://0xacab.org/leap/bitmask-core/) library is part of LEAP VPN and responsible for accessing menshen API
- To bypass censorship and access menshen, `bitmask-core`
  - connects to the private proxy `private-menshen.solitech.net:4430` using [obfs4](https://0xacab.org/leap/obfsvpn/) obfuscation
  - uses the `cert` parameter for  obfs4 proxy authentication
  - uses the `kcp` parameter to optionally enable kcp obfuscation in obfs4
  - Uses the access token `auth` for menshen authentication to get private resources
  - proxy connects to`public-menshen.solitech.net` (public menshen instance, `fqdn` parameter)

**Details:**

| part of invite code | description                                                  | examples                                                     |
| ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| url scheme          | used protocol to connect to the proxy, currently only `obfsvpnintro` is supported | obfsvpnintro://                                              |
| introducer proxy    | host or ip address of the proxy to connect. A port can be specified | private-menshen.solitech.net:4430                            |
| fqdn                | hostname of public menshen instance, needs to be a fully qualified domain name, required field, currently can be specified | public-menshen.solitech.net                                  |
| auth                | used for menshen authentication to get private resources (optional) | solitech_e9JckBd9QzYmSE4wo2jQBQ==                            |
| cert                | required for obfs4, proxy certificate for obfs4              | HtqKzIacN3+a6AAS+Xlnw/lEgHRLIm+mEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA |
| kcp                 | required for obfs4, 1 enables kcp obfuscation                | 0 or 1                                                       |
|                     |                                                              |                                                              |

All parameters need to be url encoded.

## Implementation

Working code can be found in the following repositories:

- [bitmask-core](https://0xacab.org/leap/bitmask-core/) holds the code to add invite codes (save to disk) and use them (bitmask-core is responsible for API calls to menshen). The bitmask-core library is used by the Android and Desktop clients
- The introducer proxy is part of the [obfsvpn](https://0xacab.org/leap/obfsvpn/) repository
- The backend part (authentication to get private resources) is part of [menshen](https://0xacab.org/leap/menshen). There is also a tool to generate and save invite tokens

There should be tests for each part. Please check it to get in touch with the code.

## Further information

- Discussion in https://0xacab.org/leap/bitmask-core/-/issues/27 and https://0xacab.org/leap/bitmask-core/-/merge_requests/34
