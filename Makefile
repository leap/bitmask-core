test:
	go test -count=1 -p 1 -v ./... -skip '.*Integration.*'

prepare_menshen_container:
	test -f tests/ovpn_client_ca.crt || wget --quiet https://0xacab.org/leap/menshen/-/raw/main/test/data/ovpn_client_ca.crt -O tests/ovpn_client_ca.crt
	test -f tests/ovpn_client_ca.key || wget --quiet https://0xacab.org/leap/menshen/-/raw/main/test/data/ovpn_client_ca.key -O tests/ovpn_client_ca.key
	test -f tests/ca.crt || wget --quiet https://0xacab.org/leap/menshen/-/raw/main/test/data/ca.crt -O tests/ca.crt
	test -f tests/eip-service.json || wget --quiet https://black.riseup.net/3/config/eip-service.json -O tests/eip-service.json
	test -f tests/provider.json || wget --quiet https://0xacab.org/leap/menshen/-/raw/main/test/data/provider.json -O tests/provider.json
	mkdir -p tests/db
	sudo chown 1008:1008 tests/db

integration_test: prepare_menshen_container
	sudo docker-compose -f tests/docker-compose-local.yml up -d --wait
	TEST_MENSHEN_HOST=127.0.0.1 TEST_MENSHEN_PORT=8443 TEST_MENSHEN_TLS=0 go test -count=1 -v ./... -run '.*Integration.*'
	sudo docker-compose -f tests/docker-compose-local.yml down

.PHONY: mobileclient

mobileclient:
	echo "ANDROID_NDK_HOME: $${ANDROID_NDK_HOME}" && \
	go env && \
	go get golang.org/x/mobile/cmd/gomobile@latest && \
	go install golang.org/x/mobile/cmd/gomobile@latest && \
	gomobile init && \
	gomobile bind -target='android' -ldflags="-s -w" -androidapi=21 -v -tags=netcgo -trimpath -o ./bitmask-core.aar -v ./pkg/mobile && \
	go mod tidy

