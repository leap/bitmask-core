package bootstrap

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"testing"

	"0xacab.org/leap/menshen/models"
	"github.com/stretchr/testify/assert"
)

var api5OpenvpnCertResponseBody = `
<key>
-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEICHE4KjcxowvIjzkG2OC22OePMYe2MsV2EqBboW+IcTQ
-----END PRIVATE KEY-----
</key>
<cert>
-----BEGIN CERTIFICATE-----
MIIBfTCCASOgAwIBAgIQA8mmoMwMkJaZgPr6xCG4MDAKBggqhkjOPQQDAjAzMTEw
LwYDVQQDDChMRUFQIFJvb3QgQ0EgKGNsaWVudCBjZXJ0aWZpY2F0ZXMgb25seSEp
MB4XDTI0MDkzMDEzMzgxMFoXDTI0MTAyNDEzMzgxMFowFDESMBAGA1UEAxMJVU5M
SU1JVEVEMCowBQYDK2VwAyEAHVW0D1vSIzP5ZQXDym1hfZXSKFZEvClCYEQU27Ok
c1GjZzBlMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAjAdBgNV
HQ4EFgQUEtTa8ySmBpgpo2oPEn/VW48Sp40wHwYDVR0jBBgwFoAUCZFTer2w8xkL
tpZB/y0AlfKjqAYwCgYIKoZIzj0EAwIDSAAwRQIhAOgn3r/tIRWKsjXRLcvB7EOp
zMJvaL00ZsFpi8KzR59zAiA3PoHH1kge7Q++qW8aXAOq/xdaQPfhGq/CNCjtED1M
Rw==
-----END CERTIFICATE-----
</cert>
<ca>
-----BEGIN CERTIFICATE-----
MIIBYTCCAQigAwIBAgIBATAKBggqhkjOPQQDAjAXMRUwEwYDVQQDEwxMRUFQIFJv
b3QgQ0EwHhcNMjQwMjIxMTEzMTUwWhcNMjkwMjIxMTEzNjUwWjAXMRUwEwYDVQQD
EwxMRUFQIFJvb3QgQ0EwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARKTm8AKkqK
aMI7dEarRRGEOPa3i49YE4bGNHxO97h14urXOROJWjnwHJdJ3dJk16oR0HKohXR7
jSxyukoonJkgo0UwQzAOBgNVHQ8BAf8EBAMCAqQwEgYDVR0TAQH/BAgwBgEB/wIB
ATAdBgNVHQ4EFgQUMVywfKRY9Ec3n98PVIEu7kyWKHwwCgYIKoZIzj0EAwIDRwAw
RAIgeSMNJ51+EvNJzqsISauhOTbFxiUnnmV2z/+dxYeCPzUCIEMXM/X2ekzHEz6V
l7zSfosiYvtQQL3ML3sLnVMmxdmd
-----END CERTIFICATE-----
</ca>
`

func TestMatchDelimitedString_privateKey(t *testing.T) {
	expectedKey := "-----BEGIN PRIVATE KEY-----\nMC4CAQAwBQYDK2VwBCIEICHE4KjcxowvIjzkG2OC22OePMYe2MsV2EqBboW+IcTQ\n-----END PRIVATE KEY-----\n"
	result := matchDelimitedString(api5OpenvpnCertResponseBody, keyBegin, keyEnd)
	assert.Equal(t, expectedKey, result, "private key should be parsed")
}

func TestMatchDelimitedString_ca(t *testing.T) {
	expectedCert := `-----BEGIN CERTIFICATE-----
MIIBYTCCAQigAwIBAgIBATAKBggqhkjOPQQDAjAXMRUwEwYDVQQDEwxMRUFQIFJv
b3QgQ0EwHhcNMjQwMjIxMTEzMTUwWhcNMjkwMjIxMTEzNjUwWjAXMRUwEwYDVQQD
EwxMRUFQIFJvb3QgQ0EwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARKTm8AKkqK
aMI7dEarRRGEOPa3i49YE4bGNHxO97h14urXOROJWjnwHJdJ3dJk16oR0HKohXR7
jSxyukoonJkgo0UwQzAOBgNVHQ8BAf8EBAMCAqQwEgYDVR0TAQH/BAgwBgEB/wIB
ATAdBgNVHQ4EFgQUMVywfKRY9Ec3n98PVIEu7kyWKHwwCgYIKoZIzj0EAwIDRwAw
RAIgeSMNJ51+EvNJzqsISauhOTbFxiUnnmV2z/+dxYeCPzUCIEMXM/X2ekzHEz6V
l7zSfosiYvtQQL3ML3sLnVMmxdmd
-----END CERTIFICATE-----
`
	result := matchDelimitedString(api5OpenvpnCertResponseBody, caBegin, caEnd)
	assert.Equal(t, expectedCert, result, "private key should be parsed")
}

func TestMatchDelimitedString_certificate(t *testing.T) {
	expectedCert := `-----BEGIN CERTIFICATE-----
MIIBfTCCASOgAwIBAgIQA8mmoMwMkJaZgPr6xCG4MDAKBggqhkjOPQQDAjAzMTEw
LwYDVQQDDChMRUFQIFJvb3QgQ0EgKGNsaWVudCBjZXJ0aWZpY2F0ZXMgb25seSEp
MB4XDTI0MDkzMDEzMzgxMFoXDTI0MTAyNDEzMzgxMFowFDESMBAGA1UEAxMJVU5M
SU1JVEVEMCowBQYDK2VwAyEAHVW0D1vSIzP5ZQXDym1hfZXSKFZEvClCYEQU27Ok
c1GjZzBlMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAjAdBgNV
HQ4EFgQUEtTa8ySmBpgpo2oPEn/VW48Sp40wHwYDVR0jBBgwFoAUCZFTer2w8xkL
tpZB/y0AlfKjqAYwCgYIKoZIzj0EAwIDSAAwRQIhAOgn3r/tIRWKsjXRLcvB7EOp
zMJvaL00ZsFpi8KzR59zAiA3PoHH1kge7Q++qW8aXAOq/xdaQPfhGq/CNCjtED1M
Rw==
-----END CERTIFICATE-----
`
	result := matchDelimitedString(api5OpenvpnCertResponseBody, certBegin, certEnd)
	assert.Equal(t, expectedCert, result, "private key should be parsed")
}

func TestMatchDelimitedString_duplicatedCAEnvelopeNotAllowed(t *testing.T) {
	extraCa := `
<ca>
-----BEGIN CERTIFICATE-----
ABCDEFGHIJKLMNOPQRSTUVWXYZZZZZZZZ
-----END CERTIFICATE-----
</ca>
`
	invalidCertResponseBody := fmt.Sprintf("%s%s", api5OpenvpnCertResponseBody, extraCa)
	assert.Empty(t, matchDelimitedString(invalidCertResponseBody, caBegin, caEnd))
}

func TestAPIClientModelsProviderParsing(t *testing.T) {

	// Open the JSON file
	file, err := os.Open("testdata/provider.json")
	if err != nil {
		t.Fatalf("Failed to open file: %v", err)
	}
	defer file.Close()

	// Read the file content
	data, err := io.ReadAll(file)
	if err != nil {
		t.Fatalf("Failed to read file: %v", err)
	}

	// Unmarshal the JSON data into the struct
	var p models.ModelsProvider
	if err := json.Unmarshal(data, &p); err != nil {
		t.Fatalf("Failed to unmarshal JSON: %v", err)
	}

	assert.Equal(t, true, p.Service.AllowAnonymous)
	assert.Equal(t, true, p.Service.AllowRegistration)
}
