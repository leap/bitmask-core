package bootstrap

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
}

func TestDoh(t *testing.T) {
	ip, err := dohQuery("leap.se")
	assert.NoError(t, err, "dohQuery failed")
	assert.NotNil(t, ip, "ip should not be nil")
}
