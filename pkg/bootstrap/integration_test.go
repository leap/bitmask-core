package bootstrap

import (
	"os"
	"strconv"
	"testing"

	"0xacab.org/leap/bitmask-core/pkg/introducer"
	bitmask_storage "0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/rs/zerolog/log"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func init() {
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
}

func getConfigWithoutProxy() *Config {
	host := os.Getenv("TEST_MENSHEN_HOST")
	if host == "" {
		log.Info().Msg("TEST_MENSHEN_HOST env (hostname of menshen) is not set. Using localhost as default")
		host = "localhost"
	}
	portEnv := os.Getenv("TEST_MENSHEN_PORT")
	if portEnv == "" {
		log.Info().Msg("TEST_MENSHEN_PORT env (port of menshen) env is not set. Using 8443 as default")
		portEnv = "8443"
	}
	port, err := strconv.Atoi(portEnv)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("portEnv", portEnv).
			Msg("Could not convert menshen port to int")
	}
	useTLS := os.Getenv("TEST_MENSHEN_TLS")
	if useTLS == "" {
		log.Info().Msg("TEST_MENSHEN_TLS env (connect via tls to menshen) is not set. Using default false. Set TEST_MENSHEN_TLS=1 to connect vial TLS")
		useTLS = "0"
	}
	c := NewConfig()
	c.Host = host
	c.Port = port
	c.UseTLS = useTLS == "1"
	return c
}

func getTestAPI(t *testing.T) *API {
	c := getConfigWithoutProxy()
	api, err := NewAPI(c)
	require.NoError(t, err, "Could not initialize new API")
	require.NotNil(t, api, "api should not be nil")
	return api
}

// TODO: rename after menshen enpoint was renamed/restructured
func TestIntegrationGetService(t *testing.T) {
	api := getTestAPI(t)
	service, err := api.GetService()
	assert.NoError(t, err, "Could not call getService")
	assert.NotNil(t, service, "service should not be nil")
}

func TestIntegrationGetGatewaysUnfiltered(t *testing.T) {

	api := getTestAPI(t)
	gateways, err := api.GetGateways(nil)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways returned by menshen")
}

func TestIntegrationGetGatewaysByLocation(t *testing.T) {
	api := getTestAPI(t)
	params := GatewayParams{
		Location: "paris",
	}
	gateways, err := api.GetGateways(&params)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways. Even for location paris")
	for _, gw := range gateways {
		assert.Equal(t, gw.Location, "paris", "this gateway should be located in paris")
	}
}

func TestIntegrationGetGatewaysInvalidCountryCode(t *testing.T) {
	api := getTestAPI(t)
	api.config.CountryCode = "this is invalid"
	gateways, err := api.GetGateways(nil)
	assert.Error(t, err, "GetGateways should return an error ")
	assert.Nil(t, gateways, "gateways should be nil")
}

func TestIntegrationGetGatewaysByCountryCode(t *testing.T) {
	api := getTestAPI(t)
	api.config.CountryCode = "FR"
	gateways, err := api.GetGateways(nil)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways. Even for location paris")
	for _, gw := range gateways {
		assert.Equal(t, gw.Location, "paris", "this gateway should be located in paris")
	}
}

/*
TODO: There is a logic flaw here: if we ask menshen to give us gateways based on countryCode=US,
this does not mean that the returned gateways are located in the US (only that they are near)
func TestIntegrationGetGatewaysByCountryCode(t *testing.T) {
	api := getTestAPI(t)
	params := GatewayParams{
		CC: "US",
	}
	gateways, err := api.GetGateways(&params)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways. Even with country code filtering")
	assert.Fail(t, "menshen needs support to filter for country code")
	for _, gw := range gateways {
		// TODO: Currently, a gateway does not have a field CC. Also the filtering mechanism does not work in menshen
		//assert.Equal(t, gw.Cc, "US", "this gateway should be located in the US")
	}
}
*/

func TestIntegrationGetOpenVPNCert(t *testing.T) {
	api := getTestAPI(t)
	cert, err := api.GetOpenVPNCert()
	assert.NoError(t, err, "Could not call getOpenVPNCert")
	assert.NotNil(t, cert, "cert should not be nil")
	assert.Contains(t, cert, "-----BEGIN PRIVATE KEY-----")
	assert.Contains(t, cert, "-----END PRIVATE KEY-----")
	assert.Contains(t, cert, "-----BEGIN CERTIFICATE-----")
	assert.Contains(t, cert, "-----END CERTIFICATE-----")
}

func TestIntegrationSerializeConfig(t *testing.T) {
	api := getTestAPI(t)
	openVpnConfig, err := api.SerializeConfig(nil)
	assert.NoError(t, err, "Could not call serializeConfig")
	assert.NotNil(t, openVpnConfig, "openVpnConfig should not be nil")
	assert.Contains(t, openVpnConfig, "-----BEGIN PRIVATE KEY-----")
	assert.Contains(t, openVpnConfig, "-----END PRIVATE KEY-----")
	assert.Contains(t, openVpnConfig, "-----BEGIN CERTIFICATE-----")
	assert.Contains(t, openVpnConfig, "-----END CERTIFICATE-----")
	assert.Contains(t, openVpnConfig, "remote")
	assert.Contains(t, openVpnConfig, "remote-cert-tls server")
}

// Test API communication using obfsv4 introducer. To test
// 1) git clone https://0xacab.org/leap/obfsvpn && cd obfsvpn
// 2) cp server/test_data/obfs4.json obfsvpn.yml
// 3) mkdir state
// 4) Run once: go run ./cmd/server --addr 127.0.0.1 --port 4430 --remote 127.0.0.1:8443 $(pwd)/state -v
// 5) cat state/obfs4_bridgeline.txt (there you find cert=)
// 6) go run ./cmd/server --addr 127.0.0.1 --port 4430 --remote 127.0.0.1:8443 --state $(pwd)/state --persist -v
// With --persist the cert= value is persistent/does not change.
// To use --persist, you first need to run it once without --persist to create state/obfs4_bridgeline.txt once.
// You also need menshen running on 127.0.0.1:8443 without SSL (or change via envs)
// To enable this test, set INTRODUCER_URL env:
// export INTRODUCER_URL="obfsvpnintro://localhost:4430/?cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA&fqdn=localhost&kcp=0&auth=123"
// Please make sure that the get parameters are urlencoded.
func TestIntroducerIntegrationGetService(t *testing.T) {
	introducerURL := os.Getenv("INTRODUCER_URL")
	//introducerURL = "obfsvpnintro://localhost:4430/?cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA&fqdn=localhost&auth=123"
	if introducerURL == "" {
		msg := "Skipping introducer integration test. You have to set INTRODUCER_URL env. Please check the docstring of this test"
		log.Warn().Msg(msg)
		t.Skip(msg)
	}

	c := getConfigWithoutProxy()
	c.Introducer = introducerURL
	api, err := NewAPI(c)
	require.NoError(t, err, "Could not initialize new API")
	require.NotNil(t, api, "api should not be nil")

	os.Remove("/tmp/bitmask.db")
	store, err := bitmask_storage.NewDBStore("/tmp/")
	require.NoError(t, err, "Could not create a test DB store")

	bitmask_storage.InitAppStorageWith(store)
	storage, _ := bitmask_storage.GetStorage()

	// Add Introducer
	intro, err := introducer.NewIntroducerFromURL(introducerURL)
	require.NoError(t, err, "Could not create introducer from url")

	err = storage.AddIntroducer(intro)
	require.NoError(t, err, "Could not add introducer")

	// Get Introducer by FQDN
	introducer, err := storage.GetIntroducerByFQDN("localhost")
	require.NoError(t, err, "Could not get introducer by FQDN")
	require.NotNil(t, introducer, "introducer should not be empty")

	// Use API with Introducer
	service, err := api.GetService()
	assert.NoError(t, err, "Could not call getService")
	assert.NotNil(t, service, "service should not be nil")

	// Delete Introducer
	err = storage.DeleteIntroducer("localhost")
	require.NoError(t, err, "Could not delete introducer")

	// Get non-existent Introducer by FQDN
	introducer, err = storage.GetIntroducerByFQDN("localhost")
	require.Error(t, err, "introducer should not exist")
	require.Nil(t, introducer, "introducer should be empty")

	log.Info().Msg("Introducer integration test was successfull")
}
