package bootstrap

import (
	"os"
	"testing"

	"0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func init() {
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
}

func TestValidGeolocationLookup(t *testing.T) {
	err := storage.InitAppStorage()
	require.NoError(t, err, "Could not init app storage")
	s, err := storage.GetStorage()
	require.NoError(t, err, "Could not create a test DB store")
	defer s.Close()

	api := getTestAPI(t)
	api.config.STUNServers = []string{"stun.syncthing.net:3478"}
	err = api.DoGeolocationLookup()
	assert.NoError(t, err, "Geolookup should work")
}

func TestFailedGeolocationLookup(t *testing.T) {
	err := storage.InitAppStorage()
	require.NoError(t, err, "Could not init app storage")
	s, err := storage.GetStorage()
	require.NoError(t, err, "Could not create a test DB store")
	defer s.Close()

	api := getTestAPI(t)
	// with an empty STUN server, the geolookup fails and the fallback is used
	api.config.STUNServers = []string{}
	err = api.DoGeolocationLookup()
	assert.NoError(t, err, "Geolookup will fail, but the fallback should be used. No error should be returned")
}

// Test fallback mechanism
// 1) Set fallback country code in DB to INVALIDCC
// 2) Do geolocation lookup (should work, return nil)
// 3) Check if the fallback country code in DB is valid
// Also backup and restore current stored fallback
func TestGeolocationFallback(t *testing.T) {
	err := storage.InitAppStorage()
	require.NoError(t, err, "Could not init app storage")
	s, err := storage.GetStorage()
	require.NoError(t, err, "Could not create a test DB store")
	defer s.Close()

	backupValue := s.GetFallbackCountryCode()
	s.SaveFallbackCountryCode("INVALIDCC")

	api := getTestAPI(t)
	api.config.STUNServers = []string{"stun.syncthing.net:3478"}
	err = api.DoGeolocationLookup()
	assert.NoError(t, err, "Could not fetch country code using stun.syncthing.net:3478 STUN server")

	fallbackCountyCode := s.GetFallbackCountryCode()
	assert.Equal(t, 2, len(fallbackCountyCode))

	if backupValue != "" {
		s.SaveFallbackCountryCode(backupValue)
	}
}
