package bootstrap

import (
	"testing"

	"0xacab.org/leap/bitmask-core/pkg/introducer"
	bitmask_storage "0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/stretchr/testify/require"
)

func TestGetInviteTokenAuth(t *testing.T) {
	introducerURL := "obfsvpnintro://localhost:4430/?cert=TQttsNdxY7TNiJUUZne8JYoVvOEjaEg5LaHis9V9sSNPNSxqChF2WuT19yLIM659un2xWQ&fqdn=localhost&auth=12345"

	err := bitmask_storage.InitAppStorage()
	require.NoError(t, err, "Could not init app storage")

	storage, _ := bitmask_storage.GetStorage()
	defer func() {
		storage.Close()
	}()

	err = storage.DeleteIntroducer("localhost")
	require.NoError(t, err, "Could not delete introducer")

	// no invite token/code exists, authToken should be nil
	c := getConfigWithoutProxy()
	api, err := NewAPI(c)
	require.NoError(t, err, "Could not create API")
	authToken := api.getInviteTokenAuth()
	require.Nil(t, authToken, "Auth token should be nil here")

	intro, err := introducer.NewIntroducerFromURL(introducerURL)
	require.NoError(t, err, "Could not create introducer from url")
	err = storage.AddIntroducer(intro)
	require.NoError(t, err, "Could not add introducer")

	c.Introducer = introducerURL
	api, err = NewAPI(c)
	require.NoError(t, err, "Could not create API the second time")

	// invite token/code exists, authToken should be runtime.ClientAuthInfoWriter
	authToken = api.getInviteTokenAuth()
	require.NotNil(t, authToken, "Auth token should not be nil here")
}
