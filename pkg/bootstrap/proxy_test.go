package bootstrap

import (
	"os"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

func TestValidProxyConfig(t *testing.T) {
	c := getConfigWithoutProxy()
	c.Proxy = "socks5://localhost:9050"
	_, err := NewAPI(c)
	assert.NoError(t, err, "This should be a valid proxy configuration")
}

func TestInvalidProxyConfig(t *testing.T) {
	c := getConfigWithoutProxy()
	c.Proxy = "ThisMissesAScheme"
	_, err := NewAPI(c)
	assert.ErrorContains(t, err, "unknown scheme")
	log.Info().Msgf("%v", err)
}

func getTestAPIWithTorProxy(t *testing.T) *API {
	c := getConfigWithoutProxy()
	c.Proxy = "socks5://localhost:9050"
	api, err := NewAPI(c)
	assert.NoError(t, err, "Could not initialize new API")
	assert.NotNil(t, api, "api should not be nil")
	return api
}
func TestIntegrationGetGatewaysUnfilteredWithTor(t *testing.T) {
	// if TEST_MENSHEN_HOST is not set, the default is localhost
	menshenHost := os.Getenv("TEST_MENSHEN_HOST")
	menshenNoTLS := os.Getenv("TEST_MENSHEN_TLS") == "0"
	if menshenHost == "" || menshenHost == "127.0.0.1" || menshenHost == "localhost" || menshenNoTLS {
		t.Skip("Can't use tor with a menshen instance running on localhost")
	}
	api := getTestAPIWithTorProxy(t)
	gateways, err := api.GetGateways(nil)
	assert.NoError(t, err, "unknown error general SOCKS error")
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways returned by menshen")
}
