package storage

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/rand"
)

func TestNewDBStore(t *testing.T) {
	r := rand.New(rand.NewSource(uint64(time.Now().Unix())))

	randomInt := r.Int()
	path := fmt.Sprintf("/tmp/bitmask-core-test/%d", randomInt)
	_, err := os.Open(path)
	require.NotNil(t, err)

	store, err := NewDBStore(path)
	assert.Nil(t, err)
	assert.NotNil(t, store)

	_ = os.RemoveAll("/tmp/bitmask-core-test")
}
