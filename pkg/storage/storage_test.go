// storage provides an embeddable database that bitmask uses to persist
// a series of values.
// For now, we store the following items in the database:
// - Introducer metadata.
// - Private bridges.
package storage

import (
	"os"
	"testing"

	"0xacab.org/leap/bitmask-core/pkg/introducer"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type TestStore struct {
	Store,
	stringMap map[string]string
	booleanMap   map[string]bool
	intMap       map[string]int
	longMap      map[string]int64
	byteArrayMap map[string][]byte
}

func (s *TestStore) GetString(key string) string                             { return s.stringMap[key] }
func (s *TestStore) GetStringWithDefault(key string, value string) string    { return value }
func (s *TestStore) GetBoolean(key string) bool                              { return s.booleanMap[key] }
func (s *TestStore) GetBooleanWithDefault(key string, value bool) bool       { return value }
func (s *TestStore) GetInt(key string) int                                   { return s.intMap[key] }
func (s *TestStore) GetIntWithDefault(key string, value int) int             { return value }
func (s *TestStore) GetLong(key string) int64                                { return s.longMap[key] }
func (s *TestStore) GetLongWithDefault(key string, value int64) int64        { return value }
func (s *TestStore) GetByteArray(key string) []byte                          { return s.byteArrayMap[key] }
func (s *TestStore) GetByteArrayWithDefault(key string, value []byte) []byte { return value }
func (s *TestStore) SetString(key string, value string)                      {}
func (s *TestStore) SetBoolean(key string, value bool)                       {}
func (s *TestStore) SetInt(key string, value int)                            {}
func (s *TestStore) SetLong(key string, value int64)                         {}
func (s *TestStore) SetByteArray(key string, value []byte)                   {}
func (s *TestStore) Contains(key string) (bool, error)                       { return false, nil }
func (s *TestStore) Remove(key string) error                                 { return nil }
func (s *TestStore) Clear() error                                            { return nil }
func (s *TestStore) Close() error                                            { return nil }
func (s *TestStore) Open() error                                             { return nil }

func TestNewStorageWithDefaultsDir(t *testing.T) {
	storage, err := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()
	assert.Nil(t, err)
	assert.NotNil(t, storage)
	assert.NotNil(t, storage.store)
}

func TestNewStorageWithStore(t *testing.T) {
	storage := NewStorageWithStore(&TestStore{})
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()
	assert.NotNil(t, storage)
	assert.NotNil(t, storage.store)
}

func TestListBridges_ReturnsEmptySlice(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	bridges, err := storage.ListBridges()
	assert.Nil(t, err)
	assert.NotNil(t, bridges)
	assert.Empty(t, bridges)
}

func TestListIntroducers_ReturnsEmptySlice(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	introducers, err := storage.ListIntroducers()
	assert.Nil(t, err)
	assert.NotNil(t, introducers)
	assert.Empty(t, introducers)
}

func TestAddIntroducerWithDefaultStore(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	intro1, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.4:555/?fqdn=example.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 1")
	err = storage.AddIntroducer(intro1)
	require.NoError(t, err, "Could not add introducer 1")

	intro2, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.5:455/?fqdn=example2.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 2")
	err = storage.AddIntroducer(intro2)
	require.NoError(t, err, "Could not add introducer 2")

	intros, err := storage.ListIntroducers()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(intros))

	_ = storage.store.Clear()
	intros, err = storage.ListIntroducers()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(intros))
}

func TestNewIntroducer_DeleteExistingEntriesWithSameFQDN(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	intro1, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.4:455/?fqdn=example.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 1")
	err = storage.AddIntroducer(intro1)
	require.NoError(t, err, "Could not add introducer 1")

	intro2, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.4:455/?fqdn=example.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 2")
	err = storage.AddIntroducer(intro2)
	require.NoError(t, err, "Could not add introducer 2")

	intros, err := storage.ListIntroducers()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(intros))
}

func TestGetIntroducerByFQDN(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	err := storage.DeleteIntroducer("example.org")
	require.NoError(t, err, "Could not delete existing introducers for fqdn=example.org")
	err = storage.DeleteIntroducer("example2.org")
	require.NoError(t, err, "Could not delete existing introducers for fqdn=example2.org")

	intro1, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.4:455/?fqdn=example.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 1")
	err = storage.AddIntroducer(intro1)
	require.NoError(t, err, "Could not add introducer 1")

	intro2, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.5:455/?fqdn=example2.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 2")
	err = storage.AddIntroducer(intro2)
	require.NoError(t, err, "Could not add introducer 2")

	intro, err := storage.GetIntroducerByFQDN("example.org")
	assert.NoError(t, err, "Could not get introducer by fqdn")
	assert.NotNil(t, intro)
	assert.Equal(t, "example.org", intro.FQDN)
	assert.Equal(t, "obfsvpnintro://1.2.3.4:455/?cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA&fqdn=example.org&kcp=0&auth=", intro.URL())
}

func TestDeleteIntroducerByFQDN(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	intro1, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.4:455/?fqdn=example.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 1")
	err = storage.AddIntroducer(intro1)
	require.NoError(t, err, "Could not add introducer 1")

	intro2, err := introducer.NewIntroducerFromURL("obfsvpnintro://1.2.3.5:455/?fqdn=example2.org&cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA")
	require.NoError(t, err, "Could not create introducer 2")
	err = storage.AddIntroducer(intro2)
	require.NoError(t, err, "Could not add introducer 2")

	intros, _ := storage.ListIntroducers()
	assert.Equal(t, 2, len(intros))
	err = storage.DeleteIntroducer("example.org")
	assert.Nil(t, err)
	intros, _ = storage.ListIntroducers()
	assert.Equal(t, 1, len(intros))
	assert.Equal(t, "example2.org", intros[0].FQDN)
}

func TestNewBridge(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	rawString := "{\"healthy\":true,\"host\":\"cod.demo.bitmask.net\",\"ip_addr\":\"37.218.245.94\",\"ip6_addr\":\"\",\"load\":0,\"location\":\"Paris\",\"overloaded\":false,\"port\":443,\"transport\":\"tcp\",\"type\":\"obfs4\",\"options\":{\"cert\":\"XXX\",\"iatMode\":\"0\"},\"bucket\":\"\"}"
	err := storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	assert.Nil(t, err)
	bridges, err := storage.ListBridges()
	assert.Nil(t, err)
	assert.NotNil(t, bridges)
	assert.NotEmpty(t, bridges)
}

func TestNewBridge_RejectDuplicates(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	rawString := "{\"healthy\":true,\"host\":\"cod.demo.bitmask.net\",\"ip_addr\":\"37.218.245.94\",\"ip6_addr\":\"\",\"load\":0,\"location\":\"Paris\",\"overloaded\":false,\"port\":443,\"transport\":\"tcp\",\"type\":\"obfs4\",\"options\":{\"cert\":\"XXX\",\"iatMode\":\"0\"},\"bucket\":\"\"}"
	err := storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	assert.Nil(t, err)
	// duplicated entry - name and raw string
	err = storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	assert.NotNil(t, err)
	// duplicated name
	err = storage.NewBridge("bridge1", "obfs4", "Paris", "")
	assert.NotNil(t, err)
	// duplicated rawString
	err = storage.NewBridge("bridge2", "obfs4", "Paris", rawString)
	assert.NotNil(t, err)

	bridges, _ := storage.ListBridges()
	assert.Equal(t, 1, len(bridges))
}

func TestGetBridgeByName(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	rawString := "{\"healthy\":true,\"host\":\"cod.demo.bitmask.net\",\"ip_addr\":\"37.218.245.94\",\"ip6_addr\":\"\",\"load\":0,\"location\":\"Paris\",\"overloaded\":false,\"port\":443,\"transport\":\"tcp\",\"type\":\"obfs4\",\"options\":{\"cert\":\"XXX\",\"iatMode\":\"0\"},\"bucket\":\"\"}"
	_ = storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	_ = storage.NewBridge("bridge2", "obfs4", "Paris", "")

	bridges, _ := storage.ListBridges()
	assert.Equal(t, 2, len(bridges))

	bridge, err := storage.GetBridgeByName("bridge2")
	assert.Nil(t, err)
	assert.Equal(t, "", bridge.Raw)
}

func TestGetBridgesByLocation(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	rawString := "{\"healthy\":true,\"host\":\"cod.demo.bitmask.net\",\"ip_addr\":\"37.218.245.94\",\"ip6_addr\":\"\",\"load\":0,\"location\":\"Paris\",\"overloaded\":false,\"port\":443,\"transport\":\"tcp\",\"type\":\"obfs4\",\"options\":{\"cert\":\"XXX\",\"iatMode\":\"0\"},\"bucket\":\"\"}"
	_ = storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	_ = storage.NewBridge("bridge2", "obfs4", "Paris", "bridge2_raw")
	_ = storage.NewBridge("bridge3", "obfs4", "Prague", "bridge3_raw")

	bridges, err := storage.GetBridgesByLocation("Paris")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(bridges))

	assert.Equal(t, "bridge1", bridges[0].Name)
	assert.Equal(t, "bridge2", bridges[1].Name)
}

func TestGetBridgesByType(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	rawString := "{\"healthy\":true,\"host\":\"cod.demo.bitmask.net\",\"ip_addr\":\"37.218.245.94\",\"ip6_addr\":\"\",\"load\":0,\"location\":\"Paris\",\"overloaded\":false,\"port\":443,\"transport\":\"tcp\",\"type\":\"obfs4\",\"options\":{\"cert\":\"XXX\",\"iatMode\":\"0\"},\"bucket\":\"\"}"
	_ = storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	_ = storage.NewBridge("bridge2", "obfs4", "Paris", "bridge2_raw")
	_ = storage.NewBridge("bridge3", "hoppingPt", "Prague", "bridge3_raw")

	bridges, err := storage.GetBridgesByType("obfs4")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(bridges))

	assert.Equal(t, "bridge1", bridges[0].Name)
	assert.Equal(t, "bridge2", bridges[1].Name)
}

func TestDeleteBridgeByName(t *testing.T) {
	storage, _ := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()

	rawString := "{\"healthy\":true,\"host\":\"cod.demo.bitmask.net\",\"ip_addr\":\"37.218.245.94\",\"ip6_addr\":\"\",\"load\":0,\"location\":\"Paris\",\"overloaded\":false,\"port\":443,\"transport\":\"tcp\",\"type\":\"obfs4\",\"options\":{\"cert\":\"XXX\",\"iatMode\":\"0\"},\"bucket\":\"\"}"
	_ = storage.NewBridge("bridge1", "obfs4", "Paris", rawString)
	_ = storage.NewBridge("bridge2", "obfs4", "Amsterdam", "bridge2_raw")

	bridges, _ := storage.ListBridges()
	assert.Equal(t, 2, len(bridges))

	err := storage.DeleteBridge("bridge1")

	assert.Nil(t, err)
	bridges, _ = storage.ListBridges()
	assert.Equal(t, 1, len(bridges))
	assert.Equal(t, "Amsterdam", bridges[0].Location)
}

// StormDB does not allow to have mutiple DB connections
func TestMultipleDBHandlesAreDisallowed(t *testing.T) {
	storage, err := NewStorageWithDefaultDir()
	defer func() {
		_ = storage.store.Clear()
		storage.Close()
	}()
	assert.Nil(t, err)
	assert.NotNil(t, storage)
	storage2, err := NewStorageWithDefaultDir()
	assert.NotNil(t, err)
	assert.Nil(t, storage2)
}

// save country code to disk, load it from disk and compare values
func TestSavingCountryCode(t *testing.T) {
	os.Remove("/tmp/bitmask.db")
	store, err := NewDBStore("/tmp/")
	require.NoError(t, err, "Could not create a test DB store")
	defer store.Close()

	InitAppStorageWith(store)
	storage, _ := GetStorage()

	storage.SaveFallbackCountryCode("XY")

	cc := storage.GetFallbackCountryCode()
	require.Equal(t, "XY", cc)
}
