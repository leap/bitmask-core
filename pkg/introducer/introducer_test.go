// introducer is a simple obfuscated proxy that points to an instance of the menshen API.
// The upstream API can be publicly available or not.
package introducer

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewIntroducerFromURL(t *testing.T) {
	type args struct {
		introducerURL string
	}
	tests := []struct {
		name    string
		args    args
		want    *Introducer
		wantErr bool
	}{
		{
			name:    "invalid url",
			args:    args{"obfsvpnintro:invalidurl"},
			wantErr: true,
		},
		{
			name:    "cert is emtpy but mandatory",
			args:    args{"obfsvpnintro://localhost:9090/?cert=&kcp=1&fqdn=vpn.example.com&auth=123"},
			wantErr: true,
		},
		{
			name:    "fqdn is emtpy but mandatory",
			args:    args{"obfsvpnintro://localhost:9090/?cert=123&kcp=1&fqdn=&auth=123"},
			wantErr: true,
		},
		{
			name: "working obfsvpnintro introducer url",
			args: args{"obfsvpnintro://localhost:9090/?cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA&fqdn=vpn.example.com&kcp=1&auth=provider_e9JckBd9QzYmSE4wo2jQBQ%3D%3D"},
			want: &Introducer{
				Type: "obfsvpnintro",
				Addr: "localhost:9090",
				FQDN: "vpn.example.com",
				KCP:  true,
				Cert: "HtqKzIacN3+a6AAS+Xlnw/lEgHRLIm+mEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA",
				Auth: "provider_e9JckBd9QzYmSE4wo2jQBQ==",
			},
			wantErr: false,
		},
		{
			name: "kcp is turned off as default when not supplied",
			args: args{"obfsvpnintro://localhost:9090/?cert=HtqKzIacN3%2Ba6AAS%2BXlnw%2FlEgHRLIm%2BmEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA&fqdn=vpn.example.com&auth=provider_e9JckBd9QzYmSE4wo2jQBQ%3D%3D"},
			want: &Introducer{
				Type: "obfsvpnintro",
				Addr: "localhost:9090",
				FQDN: "vpn.example.com",
				KCP:  false,
				Cert: "HtqKzIacN3+a6AAS+Xlnw/lEgHRLIm+mEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA",
				Auth: "provider_e9JckBd9QzYmSE4wo2jQBQ==",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewIntroducerFromURL(tt.args.introducerURL)
			gotError := err != nil
			if gotError != tt.wantErr {
				t.Errorf("Unexpected err value after calling fromNewIntroducerFromURL() (error=%v, wantErr=%v)", err, tt.wantErr)
			}
			if !gotError {
				assert.Equal(t, got, tt.want, "NewIntroducerFromURL() returned without err, but introducer objects don't match")
			}
		})
	}
}

func TestIntroducerURL(t *testing.T) {
	intro := Introducer{
		Type: "obfsvpnintro",
		Addr: "localhost:9090",
		Cert: "HtqKzIacN3+a6AAS+Xlnw/lEgHRLIm+mEFtIgFQ26vxWa4aFy07XhWIRjK6kZ1YpxejkRA",
		FQDN: "vpn.example.com",
		KCP:  true,
		Auth: "provider_e9JckBd9QzYmSE4wo2jQBQ==",
	}

	url := intro.URL()
	intro2, err := NewIntroducerFromURL(url)
	require.NoError(t, err, "Could not create introducer from url")
	assert.Equal(t, url, intro2.URL(), "introducer url does not match")

}

func TestIntroducer_Validate(t *testing.T) {
	type fields struct {
		Type string
		Addr string
		Cert string
		FQDN string
		KCP  bool
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "happy path",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "example.com",
				KCP:  false,
			},
			wantErr: false,
		},
		{
			name: "bad type",
			fields: fields{
				Type: "obfsvpnintro2",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "example.com",
				KCP:  false,
			},
			wantErr: true,
		},
		{
			name: "addr with no port",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "example.com",
				KCP:  false,
			},
			wantErr: true,
		},
		{
			name: "no fqdn",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "",
				KCP:  false,
			},
			wantErr: true,
		},
		{
			name: "bad fqdn, no dots",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "local",
				KCP:  false,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &Introducer{
				Type: tt.fields.Type,
				Addr: tt.fields.Addr,
				Cert: tt.fields.Cert,
				FQDN: tt.fields.FQDN,
				KCP:  tt.fields.KCP,
			}
			if err := i.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("Introducer.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
