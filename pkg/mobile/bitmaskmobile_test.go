package mobile

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"testing"

	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
	"0xacab.org/leap/bitmask-core/pkg/storage"
	"0xacab.org/leap/menshen/models"

	"github.com/rs/zerolog/log"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func init() {
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
}

func getConfigWithoutProxy() *bootstrap.Config {
	host := os.Getenv("TEST_MENSHEN_HOST")
	if host == "" {
		log.Info().Msg("TEST_MENSHEN_HOST env (hostname of menshen) is not set. Using localhost as default")
		host = "localhost"
	}
	portEnv := os.Getenv("TEST_MENSHEN_PORT")
	if portEnv == "" {
		log.Info().Msg("TEST_MENSHEN_PORT env (port of menshen) env is not set. Using 8443 as default")
		portEnv = "8443"
	}
	port, err := strconv.Atoi(portEnv)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("portEnv", portEnv).
			Msg("Could not convert menshen port to int")
	}
	useTLS := os.Getenv("TEST_MENSHEN_TLS")
	if useTLS == "" {
		log.Info().Msg("TEST_MENSHEN_TLS env (connect via tls to menshen) is not set. Using default false. Set TEST_MENSHEN_TLS=1 to connect vial TLS")
		useTLS = "0"
	}
	c := bootstrap.NewConfig()
	c.Host = host
	c.Port = port
	c.UseTLS = useTLS == "1"
	return c
}

func createBitmaskMobile() *BitmaskMobile {
	c := getConfigWithoutProxy()
	urlScheme := "http"
	if c.UseTLS {
		urlScheme = "https"
	}
	bm, _ := NewBitmaskMobile(fmt.Sprintf("%s://%s:%d", urlScheme, c.Host, c.Port), NewTestStore())
	bm.config.UseTLS = c.UseTLS
	return bm
}

func TestMobileIntegrationGetService(t *testing.T) {
	bm := createBitmaskMobile()
	service, err := bm.GetService()
	assert.NoError(t, err, "Could not call getService")
	assert.NotNil(t, service, "service should not be nil")
	modelsService := models.ModelsEIPService{}
	err = json.Unmarshal([]byte(service), &modelsService)
	require.Nil(t, err)
	assert.Equal(t, "US", modelsService.Locations["miami"].CountryCode)
}

func TestMobileIntegrationGetAllBridges(t *testing.T) {
	bm := createBitmaskMobile()
	bridges, err := bm.GetAllBridges("", "", "", "")
	assert.NoError(t, err, "Could not call GetAllBridges")
	assert.NotNil(t, bridges, "bridges should not be nil")
	assert.Greater(t, len(bridges), 0)
	fmt.Printf("bridges: %v\n", bridges)
}

func TestMobileIntegrationGetAllGateways(t *testing.T) {
	bm := createBitmaskMobile()
	gateways, err := bm.GetAllGateways("", "", "")
	assert.NoError(t, err, "Could not call GetGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 0)
	fmt.Printf("gateways: %v\n", gateways)
}

func TestMobileIntegrationGetProvider(t *testing.T) {
	bm := createBitmaskMobile()
	providerString, err := bm.GetProvider()
	assert.NoError(t, err, "Could not call GetProvider")
	assert.NotNil(t, providerString, "provider should not be nil")
	provider := models.ModelsProvider{}
	err = json.Unmarshal([]byte(providerString), &provider)
	require.Nil(t, err)
	assert.Equal(t, provider.Domain, "demo.bitmask.net")
	assert.Equal(t, true, provider.Service.AllowAnonymous)
	assert.Equal(t, true, provider.Service.AllowRegistration)
}

func TestMobileTestSetIntroducer(t *testing.T) {
	bm := createBitmaskMobile()

	err := bm.SetIntroducer("https://example.com")
	assert.NotNil(t, err)

	introURL := "obfsvpnintro://localhost:4433/?cert=abcdef7890123456879012345678901234568790123456789012345678901234567890&fqdn=example.com&kcp=0&auth=0"
	err = bm.SetIntroducer(introURL)
	assert.NoError(t, err, "Could not set introducer")

	storage, err := storage.GetStorage()
	require.Nil(t, err)
	defer storage.Close()
	intro, err2 := storage.GetIntroducerByFQDN("example.com")

	assert.Nil(t, err2)

	assert.Equal(t, introURL, intro.URL())
	assert.NotEmpty(t, intro.FQDN)

	assert.Equal(t, introURL, bm.config.Introducer)
}

type TestStore struct {
	Store,
	stringMap map[string]string
	booleanMap   map[string]bool
	intMap       map[string]int
	longMap      map[string]int64
	byteArrayMap map[string][]byte
}

func NewTestStore() *TestStore {
	return &TestStore{
		stringMap:    map[string]string{},
		booleanMap:   map[string]bool{},
		intMap:       map[string]int{},
		longMap:      map[string]int64{},
		byteArrayMap: map[string][]byte{},
	}
}

func (s *TestStore) GetString(key string) string                          { return s.stringMap[key] }
func (s *TestStore) GetStringWithDefault(key string, value string) string { return value }
func (s *TestStore) GetBoolean(key string) bool                           { return s.booleanMap[key] }
func (s *TestStore) GetBooleanWithDefault(key string, value bool) bool    { return value }
func (s *TestStore) GetInt(key string) int                                { return s.intMap[key] }
func (s *TestStore) GetIntWithDefault(key string, value int) int          { return value }
func (s *TestStore) GetLong(key string) int64                             { return s.longMap[key] }
func (s *TestStore) GetLongWithDefault(key string, value int64) int64     { return value }
func (s *TestStore) GetByteArray(key string) []byte                       { return s.byteArrayMap[key] }
func (s *TestStore) GetByteArrayWithDefault(key string, value []byte) []byte {
	if result := s.byteArrayMap[key]; result != nil {
		return result
	}
	return value
}
func (s *TestStore) SetString(key string, value string)    { s.stringMap[key] = value }
func (s *TestStore) SetBoolean(key string, value bool)     { s.booleanMap[key] = value }
func (s *TestStore) SetInt(key string, value int)          { s.intMap[key] = value }
func (s *TestStore) SetLong(key string, value int64)       { s.longMap[key] = value }
func (s *TestStore) SetByteArray(key string, value []byte) { s.byteArrayMap[key] = value }
func (s *TestStore) Contains(key string) (bool, error)     { return false, nil }
func (s *TestStore) Remove(key string) error               { return nil }
func (s *TestStore) Clear() error                          { return nil }
func (s *TestStore) Close() error                          { return nil }
func (s *TestStore) Open() error                           { return nil }
