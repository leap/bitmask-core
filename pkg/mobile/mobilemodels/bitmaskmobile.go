package mobilemodels

// BitmaskMobileCore defines the interface for BitmaskMobile
// it allows us to write better testable code on the other side
// of the FFI by mocking return values of BitmaskCore
type BitmaskMobileCore interface {
	SetCountryCode(cc string) error
	GetGeolocation() (string, error)
	SetUseTls(useTls bool) error
	SetResolveWithDoH(resolveWithDoH bool) error
	SetIntroducer(introducerUrl string) error
	GetIntroducerURLByDomain(domain string) (string, error)
	SetSocksProxy(proxyUrl string) error
	SetDebug(isDebug bool)
	GetProvider() (string, error)
	GetService() (string, error)
	GetBestGateway() (string, error)
	GetBestBridge() (string, error)
	GetAllBridges(location, port, transport, transportType string) (string, error)
	GetAllGateways(location, port, transport string) (string, error)
	GetOpenVPNCert() (string, error)
}
