package mobilemodels

import "0xacab.org/leap/bitmask-core/pkg/storage"

type Store interface {
	storage.Store
}
