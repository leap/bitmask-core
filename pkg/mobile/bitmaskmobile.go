package mobile

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"runtime"

	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
	"0xacab.org/leap/bitmask-core/pkg/introducer"
	"0xacab.org/leap/bitmask-core/pkg/mobile/mobilemodels"
	"0xacab.org/leap/bitmask-core/pkg/storage"
)

type BitmaskMobile struct {
	config *bootstrap.Config
}

// ensure BitmaskMobile implements BitmaskMobileCore
var _ mobilemodels.BitmaskMobileCore = &BitmaskMobile{}

func NewBitmaskMobile(url string, store mobilemodels.Store) (_ *BitmaskMobile, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16)
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	config, err := bootstrap.NewConfigFromURL(url)
	if err != nil {
		return nil, fmt.Errorf("failed to create BitmaskMobile struct: %v", err)
	}

	storage.InitAppStorageWith(store)
	bm := &BitmaskMobile{
		config: config,
	}
	return bm, nil
}

func NewBitmaskMobileWithStore(store mobilemodels.Store) (_ *BitmaskMobile, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16)
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	storage.InitAppStorageWith(store)
	bm := &BitmaskMobile{
		config: &bootstrap.Config{},
	}
	return bm, nil
}

func (bm *BitmaskMobile) SetCountryCode(cc string) error {
	if bm == nil || bm.config == nil {
		return errors.New("BitmaskMobile not initialized")
	}
	bm.config.CountryCode = cc
	return nil
}

func (bm *BitmaskMobile) GetGeolocation() (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16)
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	api, err := bootstrap.NewAPI(bm.config)
	if err != nil {
		return "", err
	}
	err = api.DoGeolocationLookup()
	if err != nil {
		return "", err
	}
	storage, err := storage.GetStorage()
	if err != nil {
		return "", err
	}
	return storage.GetFallbackCountryCode(), nil
}

func (bm *BitmaskMobile) SetUseTls(useTls bool) error {
	if bm == nil || bm.config == nil {
		return errors.New("BitmaskMobile not initialized")
	}
	bm.config.UseTLS = useTls
	return nil
}

func (bm *BitmaskMobile) SetResolveWithDoH(resolveWithDoH bool) error {
	if bm == nil || bm.config == nil {
		return errors.New("BitmaskMobile not initialized")
	}
	bm.config.ResolveWithDoH = resolveWithDoH
	return nil
}

func (bm *BitmaskMobile) SetIntroducer(introducerUrl string) (err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	intro, err := introducer.NewIntroducerFromURL(introducerUrl)
	if err != nil {
		return err
	}
	err = intro.Validate()
	if err != nil {
		return fmt.Errorf("invalid introducer: %v", err)
	}
	storage, err := storage.GetStorage()
	if err != nil {
		return err
	}
	err = storage.AddIntroducer(intro)
	if err != nil {
		return fmt.Errorf("adding introducer failed: %v", err)
	}
	bm.config.Introducer = introducerUrl
	return nil
}

func (bm *BitmaskMobile) GetIntroducerURLByDomain(domain string) (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	storage, err := storage.GetStorage()
	if err != nil {
		return "", err
	}
	result, err := storage.GetIntroducerByFQDN(domain)
	if err != nil {
		return "", err
	}

	return result.URL(), nil
}

// SetSocksProxy requires a URL in the socks5:// URL scheme
func (bm *BitmaskMobile) SetSocksProxy(proxyUrl string) error {
	if bm == nil || bm.config == nil {
		return errors.New("BitmaskMobile not initialized")
	}
	bm.config.Proxy = proxyUrl
	return nil
}

// SetDebug enables optionally logging of http requests and responses done by the
func (bm *BitmaskMobile) SetDebug(isDebug bool) {
	if isDebug {
		os.Setenv("DEBUG", "1")
		os.Setenv("GOTRACEBACK", "crash")
	} else {
		os.Setenv("DEBUG", "single")
	}
}

// GetProvider returns the essential provider infos, including urls to the API and supported API version
func (bm *BitmaskMobile) GetProvider() (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	api, err := bootstrap.NewAPI(bm.config)
	if err != nil {
		return "", err
	}

	provider, err := api.GetProvider()
	if err != nil {
		return "", err
	}

	return toJson(provider)
}

// GetService returns service information, including available locations and common tunnel config
func (bm *BitmaskMobile) GetService() (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	config := *bm.config
	api, err := bootstrap.NewAPI(&config)
	if err != nil {
		return "", err
	}
	service, err := api.GetService()
	if err != nil {
		return "", err
	}

	return toJson(service)
}

func (bm *BitmaskMobile) GetBestGateway() (string, error) {
	return "", fmt.Errorf("Not implemented yet!")
}

func (bm *BitmaskMobile) GetBestBridge() (string, error) {
	return "", fmt.Errorf("Not implemented yet!")
}

func (bm *BitmaskMobile) GetAllBridges(location, port, transport, transportType string) (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	config := *bm.config
	api, err := bootstrap.NewAPI(&config)
	if err != nil {
		return "", err
	}

	bridgeParams := &bootstrap.BridgeParams{
		Location:  location,
		Port:      port,
		Transport: transport,
		Type:      transportType,
		CC:        bm.config.CountryCode,
	}
	models, err := api.GetAllBridges(bridgeParams)
	if err != nil {
		return "", err
	}

	return toJson(models)
}

func (bm *BitmaskMobile) GetAllGateways(location, port, transport string) (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	config := *bm.config
	api, err := bootstrap.NewAPI(&config)

	if err != nil {
		return "", err
	}

	gatewayParams := &bootstrap.GatewayParams{
		Location:  location,
		Port:      port,
		Transport: transport,
	}
	models, err := api.GetGateways(gatewayParams)
	if err != nil {
		return "", err
	}

	return toJson(models)
}

func (bm *BitmaskMobile) GetOpenVPNCert() (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			buf := make([]byte, 1<<16) // 64KB buffer
			stackSize := runtime.Stack(buf, true)
			err = fmt.Errorf("recovered from panic: %v\nStack trace:\n%s", r, buf[:stackSize])
		}
	}()

	config := *bm.config
	api, err := bootstrap.NewAPI(&config)

	if err != nil {
		return "", err
	}

	cert, err := api.GetOpenVPNCert()
	if err != nil {
		return "", err
	}

	return cert, nil
}

func toJson(model any) (string, error) {
	res, err := json.Marshal(model)
	if err != nil {
		return "", err
	}
	return string(res), nil
}
