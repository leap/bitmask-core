package latency

import (
	"errors"
	"sort"
	"sync"
	"time"

	"github.com/rs/zerolog/log"

	ping "github.com/prometheus-community/pro-bing"
)

// PickBestEndpointByLatency will send a brief ICMP ping train to all
// the passed endpoints, and return the one with the lesser latency.
func PickBestEndpointByLatency(endpoints []string) (string, error) {
	ch := make(chan pingResult, len(endpoints))
	pingAllTargets(ch, endpoints)

	var pr []pingResult
	for r := range ch {
		log.Debug().Msgf("%s: min: %d, loss:%.2f", r.ip, r.stats.MinRtt.Milliseconds(), r.stats.PacketLoss)
		pr = append(pr, r)
	}
	best, err := pickBest(pr)
	return best.ip, err
}

func doPing(wg *sync.WaitGroup, ch chan pingResult, ip string) {
	defer wg.Done()
	pinger := ping.New(ip)
	pinger.Interval = time.Millisecond * 200
	pinger.Count = 10
	pinger.Timeout = time.Second * 4
	err := pinger.Run()
	if err != nil {
		log.Warn().
			Err(err).
			Str("ip", ip).
			Msg("Could not ping host")
		return
	}
	ch <- pingResult{ip, pinger.Statistics()}
}

type pingResult struct {
	ip    string
	stats *ping.Statistics
}

func pingAllTargets(ch chan pingResult, targets []string) {
	var wg sync.WaitGroup
	for _, ip := range targets {
		wg.Add(1)
		go doPing(&wg, ch, ip)
	}
	wg.Wait()
	close(ch)
}

func pickBest(pr []pingResult) (pingResult, error) {
	st := pr[:]
	log.Debug().Msgf("results: %d", len(st))

	var noloss []pingResult
	for _, val := range st {
		if val.stats.PacketLoss == 0 {
			noloss = append(noloss, val)
		}
	}
	// if everything is loss, we might want to set a loss threshold.
	// ignoring the loss filter for now.
	if len(noloss) != 0 {
		st = noloss
	}
	log.Debug().Msgf("no-loss: %d", len(st))

	sort.Slice(st, func(i, j int) bool {
		return st[i].stats.MinRtt < st[j].stats.MinRtt
	})

	if len(st) == 0 {
		return pingResult{}, errors.New("Could not pick best result. Ping results are empty")
	}
	return st[0], nil
}
