// latency offers functions to sort a list of gateways (or in the future,
// bridges) by latency from the client's point of view.
//
// This will not be exposed in the mainstream clients yet, since
// we're unsure how the manual override will interact with the automated
// gateway selection, and a mass-deployment of the load balancer is still
// not in sight.
//
// That said, it can be useful to allow advanced users to easily check by
// themselves if the gateways suggested by menshen are indeed what they can
// reach with a lesser latency.
//
// The android and desktop clients can also try to schedule periodic
// measurements whenever they're in an OFF state. We need to make sure to pass
// them only non-overloaded gateways.
package latency
